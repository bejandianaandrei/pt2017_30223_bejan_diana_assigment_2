public class Client {
	private int id;
	private int arrivalTime;//timp inceput;
	private int serviceTime;//minute;
	private int finalTime;//timp finalizare;
	
 Client(int id, int delay, int wait, int time){
	 this.id=id;
	 this.arrivalTime=delay;// Timpul diferenta fata de timpul real
	 // ( Depinde cati de multi clienti sau procesat pana acum)
	 this.serviceTime=time;
	 this.finalTime=this.arrivalTime+(this.serviceTime+wait);
 }
 public int getClientId(){
	 return this.id;
 }
public int getServiceTime(){
	return this.serviceTime;
}
public int getArrivalTime(){
	return this.arrivalTime;
}
public int getFinalTime(){
	return this.finalTime;
}
//Plus Timp de asteptare
public int getDelayTime(){
	return this.finalTime - this.arrivalTime - this.serviceTime;
}
public String convertToString(){
	String mvenit,ovenit, mplecat, oplecat;
	if(arrivalTime%60<10) mvenit="0"+arrivalTime%60;
	 else mvenit=""+(arrivalTime%60);
	 if((arrivalTime/60)+8<10) ovenit="0"+(arrivalTime/60)+8;
	 else ovenit=""+(arrivalTime/60);
	if(finalTime%60<10) mplecat="0"+finalTime%60;
	else mplecat=""+finalTime%60;
	if((finalTime/60)+8<10) oplecat="0"+(finalTime/60+8);
	else oplecat=""+(finalTime/60);
	return ("Client nr "+(id+1)+", arrival time: "+(ovenit)+":"+mvenit+", final time: "+(oplecat)+
			":"+mplecat+", serviceTime: "+serviceTime +", waiting time "+this.getDelayTime()+";\n");
}

}
