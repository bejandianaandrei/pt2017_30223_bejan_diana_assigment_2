import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by diana on 01.04.2017.
 */
public class Controller {
    private View view;
    private Store store;
    private int numOfQueues, minutesPDay;
    private boolean type;

    public Controller(View view) {
        this.view = view;
        this.addButtonListener();
    }

    private void addButtonListener(){
        this.view.addStartButtonActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

               minutesPDay=view.getMinutesPerDay();
               numOfQueues=view.getNumberOfQueues();
               type=view.shortestTimeStrategy();
               store=new Store(view,minutesPDay,numOfQueues, type);
               view.setStore(store);
                store.addObserver(view);



            }
        });


    }
}
