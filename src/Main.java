/**
 * Created by diana on 02.04.2017.
 */
public class Main {
    private View view ;
    private Controller controller;
    public Main(){
        view = new View();
        controller=new Controller(view);
    }
    public static void main(String args[]){
        new Main();
    }

}
