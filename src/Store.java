import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


public class Store extends Observable{
	private int minutesPerDay;//minim 300, maxim 420
	private ArrayList<StoreQueue> storeQueues;
	private int numberOfClients;//minim 100, maxim 200;
	private int numberOfQueues;//poate fi minim o  coada si maxim 5 cozi;
    private boolean shortestTimeStrategy;
    private View view;

	public Store(View view, int minutesPerDay, int numberOfQueues, boolean shortestTimeStrategy) {
	    this.view=view;
        this.shortestTimeStrategy=shortestTimeStrategy;
		this.minutesPerDay = minutesPerDay;
		this.numberOfClients = 0;
		this.numberOfQueues = numberOfQueues ;
		this.storeQueues= new ArrayList<StoreQueue>();
		this.addClients();
	}

    public void setCChanged(int i){

	    this.setChanged();
	    this.notifyObservers(i);
    }
	public Store getStore(){
	    return this;
    }
    public ArrayList<StoreQueue> getStoreQueues(){
        return this.storeQueues;
    }

	public void addClients()  {
        int numberOfClients =  this.minutesPerDay/5;
        if (this.shortestTimeStrategy) {
            for (int i = 0; i < this.numberOfQueues; i++) {
                int j = 0;
                this.storeQueues.add(new StoreQueue(this,Integer.toString(i), this.minutesPerDay));

                 numberOfClients = 0;
                boolean var = true;
                while (var) {
                    j = (int) (Math.random() * 10000);
                    int time = (int) (Math.random() * 100) % 5 + 1;
                    int wait = (int) (Math.random() * 100) % 4;
                    storeQueues.get(i).addClient(new Client(j, (storeQueues.get(i).getWaitingTime()), wait, time));
                    if (storeQueues.get(i).getClientList().get(numberOfClients).getFinalTime() > storeQueues.get(i).getEndTime()+10) {
                        storeQueues.get(i).getClientList().remove(numberOfClients);
                        var = false;
                    }
                    numberOfClients++;
                }
            }
        } else {
            for (int i = 0; i < this.numberOfQueues; i++) {
                int j = 0;
                this.storeQueues.add(new StoreQueue(this,Integer.toString(i), this.minutesPerDay));

                boolean var = true;
                while (numberOfClients > 0) {
                    j = (int) (Math.random() * 10000);
                    int time = (int) (Math.random() * 100) % 5 + 1;
                    int wait = (int) (Math.random() * 100) % 4;
                    storeQueues.get(i).addClient(new Client(j, (storeQueues.get(i).getWaitingTime()), wait, time));
                    numberOfClients--;
                }
            }
        }
        for (int i = 0; i < this.numberOfQueues; i++)
            this.storeQueues.get(i).start();

    }



}

