import java.io.File;
import java.util.ArrayList;
import java.util.Observable;


public class StoreQueue extends Observable implements Runnable {
    private String id;
    private ArrayList<Client> queue;
    private int waitingTime;
    private boolean status;
    private Thread thread;
    private int startTime;
    private int endTime;
    private int openTime;
    private Store parent;
    public static File file = new File("Client.txt");
    // Writes the content to the file
    //New queue is empty, but is is opened

    public StoreQueue(Store parent, String id, int openTime) {
        this.parent=parent;
        this.id = ""+ (Integer.parseInt(id)+1);
        this.openTime = openTime;
        this.startTime = 0;
        this.endTime = this.startTime + openTime;
        this.queue = new ArrayList<Client>();
        this.waitingTime = 0;
        this.status = true;

    }

    public int getStartTime() {
        return this.startTime;
    }

    public int getEndTime() {
        return this.endTime;
    }

    public int getOpenTime() {
        return this.openTime;
    }

    public String getId() {
        return this.id;
    }

    public void addClient(Client c) {
        this.queue.add(c);
        this.updateWaitingTime(c.getServiceTime() + c.getDelayTime());
    }

    public void deleteClient(Client c) {
        if(this.getClientList().size()==0)return;
        c=queue.get(0);
        this.queue.remove(c);
        this.updateWaitingTime(-1 * (c.getServiceTime() + c.getDelayTime()));
    }

    public ArrayList<Client> getClientList() {
        return this.queue;
    }

    public void updateWaitingTime(long minute) {
        this.waitingTime += minute;
    }

    public int getWaitingTime() {
        return this.waitingTime;
    }

    public void setOpen(boolean status) {
        this.status = status;
    }

    public boolean isOpen() {
        return this.status;
    }

    public void run() {
        int numberOfClients = 0, waitingTime = 0, serviceTime = 0;
        while (!queue.isEmpty()) {
            try {

                Client c = queue.get(0);
                numberOfClients++;
                waitingTime += c.getDelayTime();
                serviceTime += c.getServiceTime();
                Thread.sleep((c.getServiceTime() + c.getDelayTime()) *500);
                this.endTime=c.getFinalTime();
                this.deleteClient(c);
                parent.setCChanged(Integer.parseInt(this.id)-1);
                System.out.print("Queue " + (this.id)+ " Processing " + c.convertToString());
                System.out.println("Client nr " + (c.getClientId()+1)+ " is quiting from queue " + (this.id));
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        String  mplecat, oplecat;


        if(this.getEndTime()%60<10) mplecat="0"+this.getEndTime()%60;
        else mplecat=""+this.getEndTime()%60;
        if((this.getEndTime()/60)+8<10) oplecat="0"+(this.getEndTime()/60+8);
        else oplecat=""+(this.getEndTime()/60);
        System.out.printf("End %s : number of Clients %d, averge waiting time %.1f, average service time %.1f\n"+
                "Start : 08:00 \n End : %s:%s \n",this.id,
                numberOfClients, (float) waitingTime / numberOfClients, (float) serviceTime / numberOfClients,
              oplecat,mplecat);
    }

    public void start() {
        System.out.println("Openning " + this.id+" at 08:00");
        if (thread == null) {
            thread = new Thread(this, this.id);
            thread.start();
        }
    }


}