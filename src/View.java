import javax.swing.*;
import javax.swing.plaf.ViewportUI;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


import static java.awt.Font.ITALIC;

/**
 * Created by diana on 31.03.2017.
 */
public class View extends JFrame implements Observer {
    private JButton startButton;
    private JTextField minutesPerDay;
    private JTextField numberOfQueues;
    private JLabel[] storeQueues;
    private JLabel welcomeLabel, minutesLabel, queuesLabel;
    private JPanel mainJPanel, secoundPanel, thePanel;
    private JPanel[] queuesPanels;
    private Checkbox strategy;
    private Store store;

    public View() {

        setTitle("My Store");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(650, 700);
        setLayout(new GridBagLayout());
        setLocationRelativeTo(null);
        thePanel = new JPanel();
        thePanel.setLayout(new BorderLayout());
        thePanel.setBackground(new Color(230, 205, 255));
        thePanel.setPreferredSize(new Dimension(600, 600));
        secoundPanel = new JPanel(new GridLayout(1, 0));
        startButton = new JButton("Start the day");
        minutesPerDay = new JFormattedTextField();
        numberOfQueues = new JFormattedTextField();
        storeQueues = new JLabel[5];

        for (int i = 0; i < 5; i++) {
            storeQueues[i] = new JLabel("Closed" + (i + 1));
            storeQueues[i].setForeground(Color.RED);
        }
        welcomeLabel = new JLabel("Wellcome to our store!");
        minutesLabel = new JLabel("Minutes:");
        queuesLabel = new JLabel("Queues");
        mainJPanel = new JPanel();
        mainJPanel.setPreferredSize(new Dimension(100, 200));
        mainJPanel.setLayout(new GridLayout(4, 2));
        mainJPanel.add(welcomeLabel);
        mainJPanel.add(new JLabel(""));
        mainJPanel.add(minutesLabel);
        mainJPanel.add(minutesPerDay);
        mainJPanel.add(queuesLabel);
        mainJPanel.add(numberOfQueues);
        mainJPanel.add(startButton);
        strategy = new Checkbox();
        strategy.setLabel("Shortest waiting time");
        mainJPanel.add(strategy);
        queuesPanels = new JPanel[5];
        for (int i = 0; i < 5; i++) {
            queuesPanels[i] = new JPanel();
            queuesPanels[i].setBackground(new Color(255 - 40 * i, 255 - (i + 1) * 30, 255));
            queuesPanels[i].setBounds(i * 100, 0, 100, 500);
            queuesPanels[i].setLayout(new GridLayout(0, 1));
            storeQueues[i].setPreferredSize(new Dimension(80, 20));
            queuesPanels[i].add(storeQueues[i]);
            secoundPanel.add(queuesPanels[i]);
        }
        thePanel.add(secoundPanel, BorderLayout.NORTH);
        mainJPanel.setBackground(new Color(150, 255, 255));
        thePanel.add(mainJPanel, BorderLayout.SOUTH);
        add(thePanel);
        setVisible(true);
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public int getNumberOfQueues() {
        int number;
        number=Integer.parseInt(this.numberOfQueues.getText());
        while(number>5) number--;
        while(number<1) number++;
        return number;
    }

    public int getMinutesPerDay() {
        return Integer.parseInt(this.minutesPerDay.getText());
    }

    public void addStartButtonActionListener(ActionListener a) {
        this.startButton.addActionListener(a);
    }

    public boolean shortestTimeStrategy() {
        return this.strategy.isEnabled();
    }

    public void refreshQueues() {


        if(store.getStoreQueues()==null) return;
        int size = store.getStoreQueues().size();
        if (size == 0) return;
        for (int i = 0; i < size; i++) {
            queuesPanels[i].removeAll();
            queuesPanels[i].revalidate();

            storeQueues[i].setText((i + 1) + " Opened for" + store.getStoreQueues().get(i).getOpenTime());
            queuesPanels[i].add(storeQueues[i]);
            storeQueues[i].setForeground(Color.GREEN);
            if (store.getStoreQueues().get(i).getClientList().size() < 19&&store.getStoreQueues().get(i).getClientList().size()>0)
                for (Client client : store.getStoreQueues().get(i).getClientList())
                    queuesPanels[i].add(new JLabel(Integer.toString(client.getClientId() + 1)));
            else if (store.getStoreQueues().get(i).getClientList().size()>0){
                for (int j = 0; j < 19; j++) {
                    Client client = store.getStoreQueues().get(i).getClientList().get(j);
                    queuesPanels[i].add(new JLabel(Integer.toString(client.getClientId() + 1)));

                }
            }
            queuesPanels[i].revalidate();
        }

    }

    @Override
    public void update(Observable observable, Object o) {
       // this.store.getStoreQueues().get((int) o).deleteClient(new Client(0,0,0,0));
        refreshQueues();
    }
}